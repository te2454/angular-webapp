import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Authentication, CurrentUser } from 'src/app/model/authentication.model';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  public user: Authentication;
  public loginForm: FormGroup;
  submitted = false;
  showLoader = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthenticationService,
    private toastr: ToastrService,
    private globalService: GlobalService
  ) {
    this.loginForm = this.fb.group({
      username: this.fb.control(null, [Validators.required]),
      password: this.fb.control(null, [Validators.required])
    });
  }

  ngOnInit() {
  }
  ngOnDestroy() {
  }

  login() {
    this.submitted = true;
    this.user = this.loginForm.value;
    this.showLoader = true;
    if (!this.loginForm.valid) {
      this.toastr.error('Silahkan lengkapi isian login');
      this.showLoader = false;
      return;
    }
    this.authService.login(this.user).subscribe((response: any) => {
      console.log('Response Login: ');
      console.log(response);
      this.showLoader = false;
      if (response.status === 200 && response?.body?.status) {
        const token: CurrentUser = response?.body?.data;
        this.authService.setToken(token);
        this.router.navigate(['/dashboard']);
      }
      if (response.status === 400) {
        console.log('username & password salah!');
        this.toastr.warning('Username atau password salah!');
      }
    }, error => {
      this.showLoader = false;
      if (error.status === 400) {
        this.toastr.warning('Username atau password salah!');
      } else {
        this.toastr.error('Gagal login, silahkan hubungi admin', 'Server Error');
      }
      console.log(error);
    });
  }

  toRegister() {
    this.router.navigate(['register']);
  }

}
