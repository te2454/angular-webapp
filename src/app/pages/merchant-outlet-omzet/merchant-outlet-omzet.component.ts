import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { LaporanMerchantOutletOmzet } from 'src/app/model/laporan.model';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-merchant-outlet-omzet',
  templateUrl: './merchant-outlet-omzet.component.html',
  styleUrls: ['./merchant-outlet-omzet.component.scss']
})
export class MerchantOutletOmzetComponent implements OnInit {

  listMerchantOmzet: LaporanMerchantOutletOmzet[] = [];
  page = 1;
  total_record = 0
  total_page = 0
  paging_number: number[] = [];

  constructor(
    private globalService: GlobalService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.getListMerchantOmzet();
  }

  getListMerchantOmzet() {
    this.spinner.show('spinnerAmbilData');
    this.listMerchantOmzet = [];
    this.globalService.findListMerchantOutletOmzet(this.page).subscribe((response: any) => {
      console.log('Response Merchant Omzet');
      console.log(response);
      if (response.status == 200 && response?.body?.status) {
        this.total_record = response?.body?.total;
        this.total_page = Math.ceil(this.total_record / 10);
        this.paging_number = Array.from(Array(this.total_page).keys());
        this.listMerchantOmzet = response?.body?.data;
      }
      this.spinner.hide('spinnerAmbilData');
    }, error => {
      this.spinner.hide('spinnerAmbilData');
      this.toastr.error('Gagal Mendapatkan List Merchant Omzet');
      console.log(error);
    })
  }

  nextPage() {
    if (this.page > this.total_page) {
      this.page = 1;
    } else {
      this.page = this.page + 1;
    }
    this.getListMerchantOmzet();
  }

  prevPage() {
    if (this.page < 1) {
      this.page = 3;
    } else {
      this.page = this.page - 1;
    }
    this.getListMerchantOmzet();
  }

  clickPage(pageNumber) {
    this.page = pageNumber;
    this.getListMerchantOmzet();
  }

}
