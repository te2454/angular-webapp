import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import { AuthenticationService } from "./auth/authentication.service";

@Injectable({ providedIn: 'root' })
export class GlobalService {

  constructor(
    private _http: HttpClient,
    private _toast: ToastrService,
    private _router: Router,
    private _auth: AuthenticationService) {
  }

  public findListMerchantOmzet(page) {
    let params = new HttpParams();
    params = params.append('page', page);
    params = params.append('limit', 10);
    return this._http.get(`${environment.uriMajooBackend}/api/laporan/merchant-omzet`, {
      observe: 'response',
      params: params
    });
  }

  public findListMerchantOutletOmzet(page) {
    let params = new HttpParams();
    params = params.append('page', page);
    params = params.append('limit', 10);
    return this._http.get(`${environment.uriMajooBackend}/api/laporan/merchant-outlet-omzet`, {
      observe: 'response',
      params: params
    });
  }

}
