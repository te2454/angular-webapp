import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Authentication, CurrentUser } from 'src/app/model/authentication.model';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {

  private currentLogin = 'currentUser';
  jwtHelperService: JwtHelperService = new JwtHelperService();

  constructor(private http: HttpClient) {
  }

  isAuthenticated(): boolean {
    return localStorage.getItem(this.currentLogin) != null;
  }

  get currentUser(): CurrentUser {
    const currentLogin: CurrentUser = JSON.parse(this.isAuthenticated() ? localStorage.getItem(this.currentLogin) : null);
    return currentLogin;
  }

  login(auth: Authentication) {
    return this.http.post(`${environment.uriMajooBackend}/api/auth/login`, auth, { observe: 'response' });
  }

  setToken(currentUser: CurrentUser): void {
    localStorage.setItem(this.currentLogin, JSON.stringify(currentUser));
  }
}
