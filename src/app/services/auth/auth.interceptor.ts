import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { catchError } from 'rxjs/operators';

// declare var require;
// const Swal = require('sweetalert2');

@Injectable()
export class AuthenticationInterceptor implements HttpInterceptor {
  constructor(
    private loginService: AuthenticationService,
    private toast: ToastrService,
    private spinner: NgxSpinnerService,
    private router: Router) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    let header = new HttpHeaders();
    const urlBased = req.url;

    if (this.loginService.currentUser) {
      header = header.append('Authorization', `${this.loginService.currentUser.token}`);
      if (!urlBased.match('\/(login)\/[a-zA-Z0-9.?=&&_-]*')) {
        const newRequest = req.clone({
          headers: header
        });
        return next.handle(newRequest).pipe(catchError((error: HttpErrorResponse) => {
          let errorMessage = '';
          if (error.error instanceof ErrorEvent) {
            // client-side error
            errorMessage = 'Tidak Terdefinisi';
          } else {
            // server-side error
            // errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
            if (error.status === 502) {
              errorMessage = `Service Mati.`;
            } else if (error.status === 504) {
              errorMessage = `Service Mati Atau Waktu Koneksi Habis.`;
            } else if (error.status === 500) {
              errorMessage = `Service error belum terdefinisi.`;
            } else if (error.status === 400) {
              errorMessage = `${error?.error?.message}`;
            } else if (error.status === 404) {
              errorMessage = `Service Tidak Ditemukan`;
            } else {
              errorMessage = 'Tidak Terdefinisi';
            }
          }
          // Swal.fire(
          //   'Terjadi Kesalahan',
          //   errorMessage,
          //   'error'
          // );
          this.toast.error(errorMessage, 'Terjadi Kesalahan');
          return throwError(errorMessage);
        }));
      } else {
        const newRequest = req.clone();
        return next.handle(newRequest);
      }
    } else {
      const newRequest = req.clone();
      return next.handle(newRequest);
    }
  }
}
