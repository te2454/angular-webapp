export class UserRegister {
  name?: string;
  username?: string;
  password?: string;
}

export class UserUpdate {
  id?: number;
  name?: string;
  user_name?: string;
}
