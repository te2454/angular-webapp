export class LaporanMerchantOmzet {
  MerchantName?: any;
  Omzet?: any;
  OmzetDate?: any;
}

export class LaporanMerchantOutletOmzet {
  MerchantName?: any;
  OutletName?: any;
  Omzet?: any;
  OmzetDate?: any;
}
