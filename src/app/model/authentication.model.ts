export interface Authentication {
  username?: string;
  password?: string;
}

export class CurrentUser {
  id?: any;
  name?: any;
  user_name?: any;
  created_by?: any;
  created_at?: any;
  updated_by?: any;
  updated_at?: any;
  token?: any;
}
