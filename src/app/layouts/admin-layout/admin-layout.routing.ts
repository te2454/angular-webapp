import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { MerchantOmzetComponent } from 'src/app/pages/merchant-omzet/merchant-omzet.component';
import { MerchantOutletOmzetComponent } from 'src/app/pages/merchant-outlet-omzet/merchant-outlet-omzet.component';

export const AdminLayoutRoutes: Routes = [
  { path: 'merchant-omzet', component: MerchantOmzetComponent },
  { path: 'merchant-outlet-omzet', component: MerchantOutletOmzetComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'user-profile', component: UserProfileComponent },
  { path: 'icons', component: IconsComponent }
];
