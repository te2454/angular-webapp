import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http'; import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ClipboardModule } from 'ngx-clipboard';

import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { UserProfileComponent } from '../../pages/user-profile/user-profile.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MerchantOmzetComponent } from 'src/app/pages/merchant-omzet/merchant-omzet.component';
import { MerchantOutletOmzetComponent } from 'src/app/pages/merchant-outlet-omzet/merchant-outlet-omzet.component';
// import { ToastrModule } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    HttpClientModule,
    NgbModule,
    ClipboardModule
  ],
  declarations: [
    MerchantOmzetComponent,
    MerchantOutletOmzetComponent,
    DashboardComponent,
    UserProfileComponent,
    IconsComponent,
  ]
})

export class AdminLayoutModule { }
